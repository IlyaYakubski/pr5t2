import java.util.Arrays;

public class pr5t22 {
    public static String[] selection(String[] arrayOne) {
        int pos;
        String temp = null;
        for (int i = 0; i < arrayOne.length; i++) {
            pos = i;
            for (int j = i + 1; j < arrayOne.length; j++) {
                if (arrayOne[j].compareTo(arrayOne[pos]) < 0) {
                    pos = j;
                }
            }

            temp = arrayOne[pos];
            arrayOne[pos] = arrayOne[i];
            arrayOne[i] = temp;
        }
        return arrayOne;
    }

    public static String[] bubble(String[] arrayTwo) {
        String temp;
        boolean sort = false;
        while (!sort) {
            sort = true;
            for (int t = 0; t < arrayTwo.length - 1; t++) {
                if (arrayTwo[t].compareTo(arrayTwo[t + 1]) > 0) {
                    sort = false;
                    temp = arrayTwo[t];
                    arrayTwo[t] = arrayTwo[t + 1];
                    arrayTwo[t + 1] = temp;
                }
            }

        }
        return arrayTwo;
    }

    public static String[] insertion(String[] arrayThree) {
        for (int p = 1; p < arrayThree.length; p++) {
            String curr = arrayThree[p];
            int pr = p - 1;
            while (pr >= 0 && arrayThree[pr].compareTo(curr) > 0) {
                arrayThree[pr + 1] = arrayThree[pr];
                arrayThree[pr] = curr;
                pr--;
            }
        }
        return arrayThree;
    }

    public static void main(String[] args) {
        String[] arrayOne = {"Mao Zedong", "Grapefruit", "Anime", "Cat", "Water", "Carpet"};
        arrayOne = selection(arrayOne);
        System.out.println(Arrays.toString(arrayOne));
        String[] arrayTwo = {"Orange", "Apple", "Kim Jong Il", "Cardboard", "Lake", "Cactus"};
        arrayTwo = bubble(arrayTwo);
        System.out.println(Arrays.toString(arrayTwo));
        String[] arrayThree = {"Erich Honecker", "Chair", "Banana", "Death note", "Wall", "Ilyichs light bulb"};
        arrayThree = insertion(arrayThree);
        System.out.println(Arrays.toString(arrayThree));


    }
}